'use strict';

angular.module('amt.googleServerChannel', ['restangular']).
    factory('serverChannelRest', function (Restangular) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl('/');
        });
    }).
    factory('serverChannel', ['serverChannelRest', '$rootScope', function (serverChannelRest, $rootScope) {
        var serverChannelService = {
            _channel_cur: undefined,
            messages_len: 0,
            last_message: undefined,
            channel_tag: undefined,
            messages: [],
            is_active: function () {
                return this._channel_cur !== undefined
            },
            init: function (channel_tag) {
                this.channel_tag = channel_tag;
                if (this.is_active()) {
                    this.close();
                }
                return this
            },
            open_by_token: function (token, on_message) {
                var self = this;
                var channel = new goog.appengine.Channel(token).open();
                channel.onmessage = function (msg) {
                    $rootScope.$apply(function () {
                        var data = JSON.parse(msg.data);
                        self.last_message = data;
                        self.messages.unshift(data);
                        self.messages = self.messages.slice(1, 10);
                        self.messages_len++;
                        if (on_message != undefined) {
                            on_message(data);
                        }
                    })
                };
                channel.onopen = function () {
                    self._channel_cur = channel;
                    $rootScope.$apply(function () {
                        self._channel_cur = channel;
                    })
                };
                channel.onclose = function () {
                    $rootScope.$apply(function () {
                        self._channel_cur = undefined;
                        self.messages = [];
                        self.messages_len = 0;
                        self.last_message = undefined;
                    })
                };
                channel.onerror = function () {

                };
                return this
            },
            open: function (account_id) {
                var self = this;
                serverChannelRest.all('channels').customPOST({account_id: account_id}, this.channel_tag).then(function (obj) {
                    var channel = new goog.appengine.Channel(obj.chanel_token).open();
                    channel.onmessage = function (msg) {
                        $rootScope.$apply(function () {
                            var data = JSON.parse(msg.data);
                            self.last_message = data;
                            self.messages.unshift(data);
                            if (self.messages.length > 10) {
                                self.messages = self.messages.slice(1, 10)
                            }
                            self.messages_len++
                        })
                    };
                    channel.onopen = function () {
                        self._channel_cur = channel;
                        $rootScope.$apply(function () {
                            self._channel_cur = channel
                        })
                    };
                    channel.onclose = function () {
                        $rootScope.$apply(function () {
                            self._channel_cur = undefined;
                            self.messages = [];
                            self.messages_len = 0;
                            self.last_message = undefined;
                        });
                    };
                    channel.onerror = function () {
                    };
                });
                return this
            },
            close: function () {
                if (this.is_active()) {
                    this._channel_cur.close();
                }
                return false
            }
        };
        $rootScope.$on('$routeChangeSuccess', function () {
            serverChannelService.close()
        });

        return serverChannelService

    }]);
